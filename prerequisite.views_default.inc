<?php
/**
 * Implementation of hook_views_default_views().
 */
// Declare all the .view files in the views subdir that end in .view
function prerequisite_views_default_views() {
  $files = file_scan_directory(drupal_get_path('module', 'prerequisite'). '/views', '.view');
  watchdog("Prereq","Scanning views");
  foreach ($files as $absolute => $file) {
  	watchdog("Prereq","Found ".$absolute);
    require $absolute;
    if (isset($view)) {
    	watchdog("Prereq","Storing view ".$view->name);
    	$views[$view->name] = $view;
    }   
  }
  return $views;
}