<?php
	/**
	* Valid permissions for this module
	* @return array An array of valid permissions for the uoy_modprereq module
	*/
	function prerequisite_perm() {
	  return array('view prerequisites','administer prerequisites');
	} // function uoy_modprereq_perm()

	function prerequisite_views_api() {
		return array(
			'api' => 2,
		);
	}
  /**
   * Implementation of hook_theme().
   */
  function prerequisite_theme() {
    return array(
      'prerequisite_admin_module_overview' => array(
        'arguments' => array('form' => NULL),
      ),
      'prerequisite_admin_programme_structure' => array(
        'arguments' => array('form' => NULL),
      ),
      'prerequisite_admin_programme_overview' => array(
        'arguments' => array('form' => NULL),
      ),
      'prerequisite_form_module_choice' => array(
        'arguments' => array('form' => NULL),
      ),
      'prerequisite_admin_programme_archive' => array(
        'arguments' => array('data'=>NULL),
      )
    );
  }

  /**
 * Implementation of hook_filter_tips().
 */
function prerequisite_filter_tips($delta, $format, $long = FALSE) {
  switch ($delta) {
    case 0:
      if ($long) { // no long version at this time
      }
      else {
        return t('Prerequisite links filter. [prerequisites] will generate a set of links for the current essential prerequisites for the module associated with the current node. [prerequisites 012345] will generate a set of links for the prerequisites for module with code 012345');
      }
      break;
    case 1:
      if ($long) { // no long version at this time
      }
      else {
        return t('Interactive programme structure links filter. [progstructure] will generate a set of links to interactive programme pages, one for each known programme. [progstructure 012345 2014/15] will generate a link to the interactive page for programme with code 012345 for the year 2014/15');
      }
      break;
  }
}
  
  function prerequisite_filter($op, $delta = 0, $format = -1, $text = '') {
    if ($op == 'list') {
      return array(
        0 => t('Prerequisite links filter'),
        1 => t('Interactive programme structure links filter')
      );
    }
    switch ($delta) {
      case 0:  // prerequisite link
        switch ($op) {
          case 'description':
            return t('A module code is replaced with a list of links to the corresponding essential prerequisite module descriptions.  Where no module code is given, the module associated with the current node is assumed');
          case 'prepare':
            return $text;
          case 'process':
            return _prerequisite_process_filter_essential_links($text);
          case 'settings':
            return _prerequisite_filter_essential_links_settings();
        }
        break;
      case 1: // progstructure links
        watchdog("Filter","progstructure filter");
        switch ($op) {
          case 'description':
            return t('A UCAS code and year is replaced with a link to the interactive programme structure for that programme.  Where no UCAS code is given, a list of all links to programme structures is inserted');
          case 'prepare':
            return $text;
          case 'process':
            return _prerequisite_process_filter_progstructure_links($text);
          case 'settings':
            return _prerequisite_filter_essential_links_settings();
        }
        break;
    }
  }

  function _prerequisite_process_filter_essential_links($text) {
    $pattern = _prerequisite_get_filter_pattern("prerequisites");   
    $matches = array();
    while (1 == ($matched = preg_match($pattern, $text, $matches))) {  
      $modulecode = trim($matches[1]);
      $repl = _prerequisite_get_prerequisite_module_links($modulecode,TRUE,TRUE);
      $text = preg_replace($pattern,$repl,$text,1);
    }
    return $text;
  }
  function _prerequisite_process_filter_progstructure_links($text) {
    $pattern = _prerequisite_get_filter_pattern("progstructure");   
    $matches = array();
    while (1 == ($matched = preg_match($pattern, $text, $matches))) {  
      $ucascode = trim($matches[1]);
      $year = trim($matches[2]);
      if(!$year) {
        watchdog("prereq_filt", "No UCASCODE");
        $year = $ucascode;
        $ucascode = FALSE;
      }
      if(!$ucascode)  {
        $repl = _prerequisite_get_all_progstructure_links($year);
      } else {
        $repl = _prerequisite_get_progstructure_link($ucascode, $year);
      }
      $text = preg_replace($pattern,$repl,$text,1);
    }
    return $text;
  }

  function _prerequisite_get_filter_pattern ($token) {
    $p = '/\[{1,2}'.$token.'\s*([\w\/]*)( [\w\/]+)?\]{1,2}/i';
    return $p;
  }

  function _prerequisite_filter_essential_links_settings() {
    $form = array();
    return $form;
  }

  function _prerequisite_get_all_progstructure_links($year) {
      $progs = prerequisite_get_programmes($year);
      $output="<ul>";
      foreach($progs as $prog) {
        
        $output.="<li>";       
        $output .= l($prog->title, 'prerequisite/'. $prog->ucascode."/".prerequisite_year_slugify($prog->year));
        $output.="</li>";
        
      }
      $output.="</ul>";
      return $output;
  }

  function _prerequisite_get_progstructure_link($code, $year) {
      $prog = prerequisite_get_programme($code, $year);
      $output .= l($prog->title, 'prerequisite/'. $prog->ucascode."/".prerequisite_year_slugify($prog->year));
      return $output;
  }

  function _prerequisite_get_prerequisite_module_links($code, $showcredits = false, $showcode = false) {
      if(!$code) {
        if (arg(0) == 'node' && is_numeric(arg(1))) 
          $nodeid = arg(1);
        $code = prerequisite_get_modcode_from_nid($nodeid);
      }
      $rls = prerequisite_rules_load($code, $nodeid);
      $rules = $rls['rules'];
      if(empty($rules)) {
        return "<p>No essential prerequisites.</p>";
      }
      $output="<ul>";
      foreach($rules as $course) {
        
        $output.="<li>";       
        $output .= l($course->title, 'node/'. $course->nid);
        if ($showcode) {
            $output .= ' '. $course->pmodcode;
        }
        if ($showcredits) {
            $output .= ' ('. $course->credits .'&nbsp;credits)';
        }
        $output.="</li>";
        
      }
      $output.="</ul>";
      return $output;
  }
 

	/**
 * Implementation of hook_menu().
 */
function prerequisite_menu() {
  $items['admin/content/prerequisite'] = array(
    'title' => 'Prerequisites',
    'description' => 'Manage maths module prerequisites',
    'page callback' => 'views_embed_view',
    'page arguments' => array('prerequisite_admin_overview'),
    'access arguments' => array('view prerequisites'),
    'file path' => drupal_get_path('module', 'views'),
    'file' => 'views.module'
  );
  $items['admin/content/prerequisite/list'] = array(
    'title' => 'List',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  $items['admin/content/prerequisite/delete'] = array(
    'title' => 'Delete prerequisite',
    'page callback' => 'prerequisite_admin_delete',
    'page arguments' => array(4),
    'access arguments' => array('administer prerequisites'),
    'type' => MENU_CALLBACK,
    'file' => 'prerequisite.admin.inc',
  );
  $items['admin/content/prerequisite/%prerequisite_rules'] = array(
    'title' => 'Edit prerequisites for module',
    'page callback' => 'prerequisite_admin_rules_module_edit',
    'page arguments' => array(3),
    'access arguments' => array('view prerequisites'),
    'type' => MENU_CALLBACK,
    'file' => 'prerequisite.admin.inc',
  );
  $items['admin/content/prerequisite/%prerequisite_rules/list'] = array(
    'title' => 'List',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  $items['admin/content/prerequisite/%prerequisite_module/add'] = array(
    'title' => 'Add prerequisite for module',
    'page callback' => 'prerequisite_admin_rule_add',
    'page arguments' => array(3),
    'access arguments' => array('administer prerequisites'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'prerequisite.admin.inc',
    'parent' => 'admin/content/prerequisite/%prerequisite_rules',
  );
  $items['admin/content/progstructure'] = array(
    'title' => 'Programme structures',
    'description' => 'Manage maths programmes\' module structure',
    'page callback' => 'prerequisite_admin_programme_overview_callback',
    'access arguments' => array('view prerequisites'),
    'file' => 'prerequisite.admin.inc',
  );

  $items['admin/content/progstructure/archive'] = array(
    'title' => 'All years',
    'description' => 'Manage maths programmes\' module structure',
    'page callback' => 'prerequisite_admin_programme_archive',
    'access arguments' => array('view prerequisites'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'prerequisite.admin.inc',
    'parent' => 'admin/content/prerequisite/%prerequisite_rules',
  );
  $items['admin/content/progstructure/archive/%prerequisite_year'] = array(
    'title' => 'Programme structures',
    'description' => 'Manage maths programmes\' module structure',
    'page callback' => 'prerequisite_admin_programme_overview_callback',
    'page arguments' => array(4),
    'access arguments' => array('view prerequisites'),
    'file' => 'prerequisite.admin.inc',
  );
  $items['admin/content/progstructure/update/%prerequisite_year'] = array(
    'title' => 'Update ',
    'description' => 'Update programmes for that year',
    'page callback' => 'prerequisite_admin_update_programmes',
    'page arguments' => array(4),
    'access arguments' => array('view prerequisites'),
    'file' => 'prerequisite.admin.inc',
  );
    $items['admin/content/progstructure/replicate/%prerequisite_year/%prerequisite_year'] = array(
    'title' => 'Update ',
    'description' => 'Update programmes for that year',
    'page callback' => 'prerequisite_admin_replicate_programmes',
    'page arguments' => array(4,5),
    'access arguments' => array('view prerequisites'),
    'file' => 'prerequisite.admin.inc',
  );
  $items['admin/content/progstructure/list'] = array(
    'title' => 'List',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );


  $items['admin/content/progstructure/add'] = array(
    'title' => 'Add programme structure',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('prerequisite_form_programme'),
    'access arguments' => array('administer prerequisites'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'prerequisite.admin.inc',
    'parent' => 'admin/content/progstructure/',
  );

  $items['admin/content/progstructure/details/%prerequisite_programme'] = array(
    'title' => 'Edit programme details',
    'page callback' => 'prerequisite_admin_progstructure_edit',
    'page arguments' => array('prerequisite_form_programme',4),
    'access arguments' => array('administer prerequisites'),
    'type' => MENU_CALLBACK,
    'file' => 'prerequisite.admin.inc',
    'parent' => 'admin/content/progstructure/',
  );

  $items['admin/content/progstructure/delete/%'] = array(
    'title' => 'Delete programme structure',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('prerequisite_progstructure_delete_form', 4),
    'access arguments' => array('administer prerequisites'),
    'type' => MENU_CALLBACK,
    'file' => 'prerequisite.admin.inc',
  );

  $items['admin/content/progstructure/%prerequisite_programme_structure'] = array(
    'title' => 'Edit structure for programme',
    'page callback' => 'prerequisite_admin_progstructure_edit',
    'page arguments' => array(3),
    'access arguments' => array('administer prerequisites'),
    'type' => MENU_CALLBACK,
    'file' => 'prerequisite.admin.inc',
  );
  
  $items['admin/settings/prerequisite'] = array(
    'title' => 'Prerequisite module settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('prerequisite_settings_form'),
    'access arguments' => array('administer prerequisites'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'prerequisite.admin.inc',
  );
  $items['admin/prerequisite/autocomplete'] = array(
      'title' => 'Autocomplete for modules',
      'page callback' => '_prerequisite_autocomplete',
      'access arguments' => array('administer prerequisites'),  
      'type' => MENU_CALLBACK,
  );
  $items['prerequisite/%/%'] = array(
    'page callback' => 'prerequisite_programme_module_choice',
    'page arguments' => array(1,2),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
    'file' => 'prerequisite.public.inc'
  );

  return $items;
}

/**************************************** AUTOCOMPLETE ***************************************************/

/**
* autocomplete helper
* $string = string for search
*/
function _prerequisite_autocomplete($string) {
  $year = prerequisite_get_current_year();
  $pryear = prerequisite_get_previous_year($year);
  $matches = array();
  // search table for module codes whose title begin with the letters the user enters in the form
  $result = db_query("SELECT m.field_modcode_value AS modcode, m.field_acadyear_value AS year, n.title AS title FROM {content_type_module} m JOIN {node} n ON m.nid=n.nid WHERE LOWER(n.title) LIKE LOWER('%%%s%%') AND (m.field_acadyear_value='%s' OR m.field_acadyear_value='%s') ORDER BY m.field_acadyear_value DESC", array($string, $year,$pryear));
  // add matches to $matches
  while ($data = db_fetch_object($result)) {
    if(!isset($matches[trim($data->modcode)])) {
      $matches[trim($data->modcode)] = check_plain(trim($data->modcode)." (".$data->year.") ".$data->title);
    }
  }
  // return for JS
  print drupal_to_js($matches);
  exit();
}

################################################################### YEARS ############################################################

function prerequisite_year_slugify($year) {
  return str_replace("/","_",$year);
}
function prerequisite_year_deslugify($year) {
  return str_replace("_","/",$year);
}
function prerequisite_year_load($year) {
  return prerequisite_year_deslugify($year);
}


function prerequisite_get_next_year($year=FALSE) {
  if(!$year) {
    $year = prerequisite_get_current_year();
  }
  $parts = explode("/",$year);
  return ($parts[0]+1)."/".($parts[1]+1);
}

function prerequisite_get_current_year() {
  static $year = FALSE;
  if($reset) {
    $year = FALSE;
  }
  if(!$year) {
    $result = db_query("SELECT MAX(field_acadyear_value) AS year FROM content_type_module");
    $data = db_fetch_object($result);
    $year = $data->year;
  }
  return $year;
}
function prerequisite_get_previous_year() {
  static $year = FALSE;
  if($reset) {
    $year = FALSE;
  }
  if(!$year) {
    $cyear = prerequisite_get_current_year();
    $result = db_query("SELECT MAX(field_acadyear_value) AS year FROM content_type_module WHERE field_acadyear_value<>'%s'",$cyear);
    $data = db_fetch_object($result);
    $year = $data->year;
  }
  return $year;
}



function prerequisite_get_available_module_years($reset=FALSE) {
  static $years = FALSE;
  if($reset) {
    $year = FALSE;
  }
  if(!$years) {
    $result = db_query("SELECT DISTINCT field_acadyear_value AS year FROM content_type_module ORDER BY field_acadyear_value");
    while($data = db_fetch_object($result)) {
      $years[$data->year]=$data->year;
    }
  }
  return $years;
}



function prerequisite_get_available_programme_years($reset=FALSE) {
  static $years = FALSE;
  if($reset) {
    $year = FALSE;
  }
  if(!$years) {
    $result = db_query("SELECT DISTINCT year FROM prerequisite_programmes ORDER BY year DESC");
    while($data = db_fetch_object($result)) {
      $years[$data->year]=$data->year;
    }
  }
  return $years;
}

########################################################################### MODULES ###############################################################

function prerequisite_get_modcode_from_nid($nid) {
  $year = prerequisite_get_current_year();
  $result = db_query("SELECT DISTINCT m.field_modcode_value AS modcode FROM {node} n LEFT JOIN {content_type_module} m ON n.vid=m.vid WHERE n.vid IS NOT NULL AND n.nid=%s", array($nid,$year));
  $obj = db_fetch_object($result);
  if($obj) return $obj->modcode;
  return FALSE;
}

function prerequisite_module_load($modcode) {
  return prerequisite_get_module($modcode);
}

function prerequisite_get_module($modcode, $reset=FALSE) {
  static $mods = array();
  if($reset) {
    $mods = array();
  }

  if (!isset($mods[$modcode])) {
    $result = db_query("SELECT DISTINCT m.field_modcode_value AS modcode, m.field_acadyear_value AS year, n.title AS title FROM {content_type_module} m LEFT JOIN {node} n ON m.nid=n.nid WHERE m.field_modcode_value='%s' ORDER BY m.field_acadyear_value DESC", $modcode);
    $mods[$modcode] = db_fetch_object($result);
    $mods[$modcode]->required = $mods[$modcode]->required?TRUE:FALSE;
  }
  return $mods[$modcode];
}



########################################################################### PREREQUISITE RULES #####################################################

function prerequisite_rules_load($modcode) {
  $year = prerequisite_get_current_year();
  $result = db_query("SELECT DISTINCT p.prid AS prid, p.pmodcode AS pmodcode, p.modcode AS modcode, p.weight AS weight, n.title, m.field_acadyear_value AS year, m.field_credits_value AS credits, m.nid FROM {prerequisite_rules} p LEFT JOIN {content_type_module} m ON p.pmodcode=m.field_modcode_value LEFT JOIN {node} n ON m.vid=n.vid WHERE n.vid IS NOT NULL AND p.modcode = '%s' AND m.field_acadyear_value='%s' ORDER BY p.weight ASC", array($modcode,$year));
  $rules['rules'] = array();
  while ($obj = db_fetch_object($result)) {
    $rules['rules'][$obj->pmodcode] = $obj; 
  }
  $rules['parent'] = prerequisite_get_module($modcode);
  return $rules;
}



function prerequisite_save_rule(&$edit) {
  global $user;
  $edit['uid'] = $user->uid;
  $edit['date_created'] = time();
  if (!empty($edit['prid']) && !empty($edit['modcode'])) {
    drupal_write_record('prerequisite_rules', $edit, 'prid');
    module_invoke_all('prerequisite', 'update', 'rule', $edit);
    $status = SAVED_UPDATED;
  }
  else if (!empty($edit['prid'])) {
    $status = prerequisite_del_rule($edit['prid']);
  }
  else {
    drupal_write_record('prerequisite_rules', $edit);
    module_invoke_all('prerequisite', 'insert', 'rule', $edit);
    $status = SAVED_NEW;
  }
  cache_clear_all();
  return $status;
}

function prerequisite_del_rule($prid) {
  $prerequisite = (array) prerequisite_get_rule($prid);
  db_query('DELETE FROM {prerequisite_rules} WHERE prid = %d', $prid);
  module_invoke_all('prerequisite', 'delete', 'rule', $prerequisite);
  cache_clear_all();
  return SAVED_DELETED;
}


function prerequisite_get_rule($prid, $reset = FALSE) {
  static $rules = array();
  if ($reset) {
    $rules = array();
  }
  if (!isset($rules[$prid])) {
    $rules[$prid] = db_fetch_object(db_query('SELECT * FROM {prerequisite_rules} WHERE prid = %d', $prid));
  }
  return $rules[$prid];
}

############################################################################# PROGRAMMES #######################################################

function prerequisite_save_programme(&$edit) {
  if (!empty($edit['pgid'])) {
    drupal_write_record('prerequisite_programmes', $edit, 'pgid');
    $status = SAVED_UPDATED;
  }
  else {
    drupal_write_record('prerequisite_programmes', $edit);
    $status = SAVED_NEW;
  }
  cache_clear_all();
  return $status;
}

function prerequisite_get_programme($ucascode, $year=FALSE) {
  if(!$year) {
    $year = prerequisite_get_current_year();
  }
  $result =  db_query('SELECT * FROM {prerequisite_programmes} WHERE ucascode = \'%s\' AND year = \'%s\'', $ucascode, $year);
  $obj = db_fetch_object($result);
  return $obj;
}

function prerequisite_programme_load($pgid, $reset = FALSE) {
  static $progs = array();
  if ($reset) {
    $progs = array();
  }
  if (!isset($progs[$pgid])) {
    $progs[$pgid] = db_fetch_object(db_query('SELECT * FROM {prerequisite_programmes} WHERE pgid = %d', $pgid));
  }
  return $progs[$pgid];
}

function prerequisite_programme_from_code_load($code,$year) {
  return prerequisite_get_programme($ucascode,$year); 
}

function prerequisite_del_programme($pgid) {
  db_query('DELETE FROM {prerequisite_programmes} WHERE pgid = %d', $pgid);
  cache_clear_all();
  return SAVED_DELETED;
}

function prerequisite_get_programmes($year=FALSE) {
  static $programmes = array();

  watchdog("Get progs",$year);
  if($reset) {
    $programmes = array();
  }
  if(!isset($programmes[$year])) {
    $programmes[$year] = array();
    $result = db_query("SELECT pgid,ucascode,title,year FROM {prerequisite_programmes} WHERE year = '%s' ORDER BY weight",$year);
    while($data = db_fetch_object($result)) {
      watchdog("Get progs",$data->ucascode);
      $programmes[$year][$data->ucascode] = $data;
    }
  }
  return $programmes[$year];
}



############################################################## COURSE CATEGORIES #############################################################

function prerequisite_get_course_categories() {
  static $cats = array();
  if(empty($cats)) {
    $result = db_query("SELECT m.field_coursecat_value AS coursecat FROM {content_type_module} m GROUP BY m.field_coursecat_value ORDER BY m.field_coursecat_value");
    while ($data = db_fetch_object($result)) {
      $cats[]=$data->coursecat;
    }
  }
  return $cats;
}

function prerequisite_get_course_category($year_int) {
  $cats = prerequisite_get_course_categories();
  return $cats[$year_int];
}

#################################################################### PROGRAMME STRUCTURES ##########################################################

function prerequisite_programme_structure_load($pgId)  {
  $programme = prerequisite_programme_load($pgId);
  $modules = prerequisite_get_programme_structure($pgId,$programme->year);
  $r = array('modules'=>$modules,'programme'=>$programme);
  watchdog("Load ps", var_export($r,TRUE));
  return $r;
}



function prerequisite_generate_programme_structure($pgid, $year) {
  $programme = prerequisite_programme_load($pgid);
  $sql=<<<EOT
INSERT IGNORE INTO {prerequisite_programme_modules} (pgid, modcode, ucascode) 
  SELECT DISTINCT $pgid, m.field_modcode_value AS modcode, '$programme->ucascode' FROM {content_type_module} m 
    LEFT JOIN {node} n ON m.vid=n.vid 
    LEFT JOIN {prerequisite_programme_modules} p ON m.field_modcode_value=p.modcode 
  WHERE n.vid IS NOT NULL AND m.field_acadyear_value='%s'
EOT;


  $result = db_query($sql, array($year));
  /* while($data = db_fetch_object($result)) {
    $edit = array('modcode'=>$data->modcode, 'ucascode'=>$ucascode, 'year'=>$year);
    drupal_write_record("prerequisite_programme_modules",$edit);
  } */
}

function prerequisite_replicate_programmes($year1, $year2) {
  $sql = <<<EOT
  INSERT INTO {prerequisite_programmes} (ucascode,duration,title,intro,weight,year) 
    SELECT ucascode,duration,title,intro,weight,'%s' 
    FROM {prerequisite_programmes} 
    WHERE year='%s'
EOT;
  $result = db_query($sql,array($year2,$year1));
}

function prerequisite_replicate_programme_structures($year1,$year2) {
  $sql = <<<EOT
  INSERT INTO {prerequisite_programme_modules} (ucascode,required,available,modcode,pgid,uid)
    SELECT pm.ucascode,pm.required,pm.available,pm.modcode,pr.pgid,uid
    FROM {prerequisite_programme_modules} pm LEFT JOIN
      {prerequisite_programmes} pr ON pm.ucascode=pr.ucascode 
    WHERE pm.year='%s' AND pr.year='%s'
EOT;
  $result = db_query($sql,array($year1,$year2));
}


function prerequisite_get_programme_structure($pgid,$year) {
  static $structure = array();
  static $generated = FALSE;
  if($reset) {
    $structure = array();
  }
  watchdog("Get progstruct","Year: ".$year);
  $sql = <<<EOT
  SELECT t.field_acadterm_value AS term, 
    t.delta AS term_delta, 
    pw.title AS pathway, 
    md.pwid AS pwid, 
    p.pmid AS pmid, 
    p.available AS available, 
    p.required AS required, 
    p.ucascode as ucascode, 
    p.pgid AS pgid,
    n.title AS title, 
    m.field_modcode_value AS modcode, 
    m.field_coursecat_value AS year 
  FROM {content_type_module} m 
    LEFT JOIN {node} n on m.vid = n.vid 
    LEFT JOIN {prerequisite_programme_modules} p ON m.field_modcode_value = p.modcode 
    LEFT JOIN {prerequisite_programmes} pr ON p.pgid = pr.pgid
    LEFT JOIN {content_field_acadterm} t ON n.vid = t.vid 
    LEFT JOIN {prerequisite_modules} md ON m.field_modcode_value=md.modcode 
    LEFT JOIN {prerequisite_programme_pathways} pw ON md.pwid=pw.pwid 
  WHERE n.vid IS NOT NULL 
    AND p.pgid=%s
    AND m.field_acadyear_value='%s' 
  ORDER BY p.available DESC, m.field_coursecat_value, t.field_acadterm_value, p.required DESC
EOT;
  if(!isset($structure[$pgid])) {
    $result = db_query($sql, array($pgid,$year));
    $structure[$pgid] = array();
    watchdog("Admin ps 2",var_export($structure,TRUE));
    while($data = db_fetch_object($result)) {
      $data_arr = (array)$data;
      if((int)$data->term_delta === 0) {
        $data->status = prerequisite_get_status_from_programme_module($data);
        $structure[$pgid][$data->modcode] = $data;
        $term = $structure[$pgid][$data->modcode]->term;
        unset($structure[$pgid][$data->modcode]->term);
        $structure[$pgid][$data->modcode]->terms = array($term=>$term);
      }
      if((int)$data->term_delta && $data->term) {
        $term = $data->term;
        $structure[$pgid][$data->modcode]->terms[$data->term]=$term;
      }
    }
    if(empty($structure[$pgid]) && !$generated) {
      unset($structure[$pgid]);
      $programme = prerequisite_get_programme($pgid);
      prerequisite_generate_programme_structure($pgid,$programme->year);
      $generated=TRUE;
      return prerequisite_get_programme_structure($pgid);//try again
    }
  }
  watchdog("Admin ps",var_export($structure,TRUE));
  return $structure[$pgid];
}


function prerequisite_get_programme_year_with_prerequisites($programme) {
  #$course_cat = prerequisite_get_course_category($year_int);
  if(!$year) {
    $year = prerequisite_get_current_year();
  }
  $sql = <<<EOT
  SELECT t.field_acadterm_value AS term, 
    t.delta AS term_delta, 
    m.field_credits_value AS credits, 
    m.field_coursecat_value AS year, 
    pw.title AS pathway, 
    md.pwid AS pwid, 
    p.pmid AS pmid, 
    p.modcode AS modcode, 
    p.available AS available, 
    p.required AS required, 
    p.ucascode as ucascode,
    pr.prid AS prid, 
    pr.pmodcode AS pmodcode,  
    n.title AS title
  FROM {content_type_module} m 
    LEFT JOIN {node} n on m.vid = n.vid 
    LEFT JOIN {prerequisite_programme_modules} p ON m.field_modcode_value = p.modcode
    LEFT JOIN {content_field_acadterm} t ON n.vid = t.vid 
    LEFT JOIN {prerequisite_rules} pr ON pr.modcode = m.field_modcode_value 
    LEFT JOIN {prerequisite_modules} md ON m.field_modcode_value=md.modcode 
    LEFT JOIN {prerequisite_programme_pathways} pw ON md.pwid=pw.pwid 
  WHERE n.vid IS NOT NULL 
    AND p.pgid=%s
    AND m.field_acadyear_value='%s' 
    AND p.available=1 AND p.ucascode <> '' 
  ORDER BY p.modcode, 
    t.delta, 
    p.available DESC, 
    m.field_coursecat_value, 
    t.field_acadterm_value, 
    p.required DESC
EOT;
  $result = db_query($sql,array($programme->pgid, $programme->year));
  $no_prerequisites = TRUE;
  $modules=array();
  $current_modcode = NULL;
  while($data = db_fetch_object($result)) {
    $new_module = FALSE;
    if($data->modcode!=$current_modcode) {
      // moved to new module
      $current_modcode = $data->modcode;
      $start_term = $data->term;
      if(!isset($terms[$data->year]))  {
        $terms[$data->year]=array();
      }
      if(!isset($terms[$data->year][$start_term])) {
        $terms[$data->year][$start_term] = array();
      }
      $terms[$data->year][$start_term][$data->modcode]=$data;
      $terms[$data->year][$start_term][$data->modcode]->prerequisites=array();
      $terms[$data->year][$start_term][$data->modcode]->terms=array();
    }
    if($data->pmodcode) {
      $terms[$data->year][$start_term][$data->modcode]->prerequisites[$data->pmodcode]=$data->pmodcode;
    }
    if($data->term) {
      $terms[$data->year][$start_term][$data->modcode]->terms[$data->term]=$data->term;
    }
  }
  return $terms;
}

function prerequisite_get_status_from_programme_module($module) {
    if($module->available==="0") {
      $status = "Unavailable";
    } elseif($module->required==="1") {
      $status = "Required";
    } else {
      $status = "Available";
    }
    return $status;
}

function prerequisite_set_available_modules($ucascode, $modcodes, $required=TRUE) {
  if($required===TRUE) {
    $record = array('modcode'=>$modcode);
    drupal_write_record('prerequisite_required', $record);
  } else {
    db_query('DELETE FROM {prerequisite_required} WHERE modcode = %s', $modcode);
  }
  return SAVED_UPDATED;
}

function prerequisite_save_programme_module(&$edit) {
  global $user;

  $edit['uid'] = $user->uid;
  $edit['date_created'] = time();

  $modedit = array('modcode'=>$edit['modcode']);
  if(!empty($edit['mid']) && !empty($edit['modcode'])) {
    $modedit['mid'] = $edit['mid'];
    drupal_write_record('prerequisite_modules', $modedit, 'mid');
  } elseif(empty($edit['mid'])) {
    drupal_write_record('prerequisite_modules', $modedit);
  }

  if (!empty($edit['pmid']) && !empty($edit['modcode']) && !empty($edit['pgid'])) {
    drupal_write_record('prerequisite_programme_modules', $edit, 'pmid');
    $status = SAVED_UPDATED;
  }
  else if (!empty($edit['pmid'])) {
    $status = prerequisite_del_programme_module($edit['pmid']);
  }
  else {
    drupal_write_record('prerequisite_programme_modules', $edit);
    $status = SAVED_NEW;
  }

  cache_clear_all();

  return $status;
}

function prerequisite_del_programme_module($pmid) {
  db_query('DELETE FROM {prerequisite_programme_modules} WHERE pmid = %d', $pmid);
  cache_clear_all();
  return SAVED_DELETED;
}




?>