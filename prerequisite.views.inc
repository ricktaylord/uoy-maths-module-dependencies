<?php
/**
 * @file
 * Provide views data and handlers for prerequisite.module
 */

/**
 * @defgroup views_prerequisite_module prerequisite.module handlers
 *
 * Includes the core 'prerequisite_rules' table
 *
 * @{
 */

/**
* Implementation of hook_views_data()
**/
function prerequisite_views_data() {
	$data['prerequisite_rules']['table']['group'] = t('Prerequisite rule');
	$data['prerequisite_rules']['table']['base'] = array(
		'field' => 'prid',
		'title' => t('Prerequisite rule'),
		'help' => t('Prerequisite rules for course modules'),
		'weight' => -10
	);
	// This table references the {node} table.
	// This creates an 'implicit' relationship to the node table, so that when 'Node'
	// is the base table, the fields are automatically available.
	$data['prerequisite_rules']['table']['join'] = array(
		// Index this array by the table name to which this table refers.
		// 'left_field' is the primary key in the referenced table.
		// 'field' is the foreign key in this table.
		'node' => array(
			'left_field' => 'nid',
			'field' => 'mnid',
		),
	);
	// Node ID field.
	$data['prerequisite_rules']['mnid'] = array(
		'title' => t('Module node with prerequisite'),
		'help' => t('This is the node of the module that holds the prerequisite'),
		// Because this is a foreign key to the {node} table. This allows us to
		// have, when the view is configured with this relationship, all the fields
		// for the related node available.
		'relationship' => array(
			'base' => 'node',
			'field' => 'nid',
			'handler' => 'views_handler_relationship',
			'label' => t('Postrequisite module node'),
		),
	);
	$data['prerequisite_rules']['pmnid'] = array(
		'title' => t('Module node with prerequisite'),
		'help' => t('This is the node of the module that is the prerequisite'),
		// Because this is a foreign key to the {node} table. This allows us to
		// have, when the view is configured with this relationship, all the fields
		// for the related node available.
		'relationship' => array(
			'base' => 'node',
			'field' => 'nid',
			'handler' => 'views_handler_relationship',
			'label' => t('Postrequisite module node'),
		),
	);
	// Example plain text field.
  $data['prerequisite_rules']['modcode'] = array(
    'title' => t('Module code'),
    'help' => t('Module code'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
	return $data;
}
?>