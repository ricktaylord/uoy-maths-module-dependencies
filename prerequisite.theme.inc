<?php
function prerequisite_theme() {
    return array(
         'prerequisite_admin_edit_prereqs' => array(
            'arguments' => array('prereq_rules' => NULL,'nid'=>NULL,'title'=>NULL),
            'template' => 'prerequisite-admin-edit-prereqs',
         )
    );
}

function template_preprocess_prerequisite_admin_edit_prereqs(&$variables) {
	drupal_add_js(drupal_get_path('module', 'prerequisite') .'/js/admin_edit_prereq.js');
}