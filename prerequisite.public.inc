<?php

function prerequisite_form_module_choice(&$form_state,$programme) {

	$form = array();
	if(!isset($form_state['storage']['programme'])) {
		//stage 1: choose course type
		$form_state['storage']['programme'] = $programme;
	}
	if(!isset($form_state['storage']['study_year'])) {
		$form_state['storage']['study_year'] = 1;
	}
	$years = prerequisite_get_programme_year_with_prerequisites($programme);

	$pre_mods = isset($form_state['storage']['selected_modules'])?$form_state['storage']['selected_modules']:array();
	$form["#terms"]=array();
	$selected_modcodes = array();
	$prerequisites = array();
	$postrequisites = array();
	$form['#modules'] = array();
	foreach($years as $yname=>$terms)  {
		$form["#years"][$yname]=array();
		foreach($terms as $tname=>$modules) {
			if($tname)  {
		      	$form["#years"][$yname]["#terms"][$tname]=array();
				$form["#years"][$yname]["#terms"][$tname]["modules"]=array();
				foreach($modules as $module) {
					$prereq_fields = array();
					$form['#modules'][$module->modcode] = $module;
					if(!empty($module->prerequisites)) {
						$prerequisites[$module->modcode] = array();

						$module->available = FALSE;

						foreach($module->prerequisites as $prereq) {
							$pmodcode = $prereq;
							$prereq_data = $form['#modules'][$prereq];
							
							if(!empty($prereq_data->title)) {
								$prereq_fields[$pmodcode] = array();
								$prereq_fields[$pmodcode]['title'] = array('#value'=> check_plain($prereq_data->title));
								$prereq_fields[$pmodcode]['term'] = array('#value'=> check_plain($prereq_data->term));
								$prereq_fields[$pmodcode]['year'] = array('#value'=> check_plain($prereq_data->year));
								$prerequisites[$module->modcode][$prereq] = $prereq;
								if(!isset($postrequisites[$prereq]))  {
									$postrequisites[$prereq] = array();
								}
								$postrequisites[$prereq][$module->modcode]= $module->modcode;
								$module->available = (isset($pre_mods[$prereq]) && $module->available)?TRUE:FALSE;
							}
						}
					} else {
						$module->available = TRUE;
					}
					if(isset($pre_mods['$modcode'])) {
						$module->chosen = TRUE;
					} else {
						$module->chosen = FALSE;
					}
					
				    $modcodes[$module->modcode] = check_plain($module->title).", ".check_plain($module->modcode)." (".check_plain($module->credits)." credits)";
				    
				    if($module->required || $module->chosen) {
				    	$selected_modcodes[$module->modcode] = $module->modcode;
				    }
				    if($module->required) {
				    	$required_modcodes[$module->modcode] = $module->modcode;
				    }
				    $form["#years"][$yname]["#terms"][$tname]["#modules"][$module->modcode]["#module"] = (array)$module;
				    $form["#years"][$yname]["#terms"][$tname]["#modules"][$module->modcode]["#prerequisites"] = $prereq_fields;
				    #$form["#years"][$yname]["#terms"][$tname]["#modules"][$module->modcode]['#title'] = array('#value' => check_plain($module->title).", ".check_plain($module->modcode));
				    #$form["#years"][$yname]["#terms"][$tname]["#modules"][$module->modcode]['modcode'] = array('#value' => check_plain($module->year));
				    #$form["#years"][$yname]["#terms"][$tname]["#modules"][$module->modcode]['credits'] = array('#value' => check_plain($module->credits));
				    $form["#years"][$yname]["#terms"][$tname]["#modules"][$module->modcode]['status'] = array('#attributes'=> array('#class' => "status "), '#value' => $module->required?"Core":"");
				    #$form["terms"][$tname]["modules"][$module->modcode]['status'] = array('#value' => check_plain($module->required?"Required":"Optional"));
				}
			}
		}
	}
	$form['#modcodes'] = $modcodes;
	$form['#selected_modcodes'] = $selected_modcodes;
	$form['#required_modcodes'] = $required_modcodes;
	$form['#prerequisites'] = $prerequisites;
	$form['#postrequisites'] = $postrequisites;
	$form['modcodes'] = array
	(
    	'#type' => 'checkboxes',
    	'#options' => $modcodes,
    	'#default_value' => $selected_modcodes,
    	#'#disabled' => $required_modcodes
	);
	if((int)$form_state['storage']['study_year']!=1) {
		// back button
		$form["back"] = array('#type' => 'submit', '#value' => t('Previous year'));
	}
	if((int)$form_state['storage']['study_year']<(int)$form_state['storage']['course_duration']){
		// forward button
		$form["forward"] = array('#type' => 'submit', '#value' => t('Next year'));
	}
	return $form;
}


function prerequisite_programme_module_choice($code, $slyear) {
	$year = prerequisite_year_deslugify($slyear);
	$programme = prerequisite_get_programme($code,$year);
  drupal_set_title(t('Interactive module choices for %title %year', array('%title' => $programme->title, '%year'=>$programme->year)));
  return drupal_get_form('prerequisite_form_module_choice' , $programme);
}


function prerequisite_form_module_choice_submit(&$form_state) {
	if(!isset($form_state['storage'['selected_modules']])) {
		$form_state['storage']['selected_modules']=array();
	}
	foreach($form_state['values']['modcodes'] as $modcode => $selected) {
		if($selected) {
			$form_state['storage']['selected_modules'][$modcode]=$modcode;
		}
	}
	if ($form_state['clicked_button']['#value'] == t('Previous year')) {
		$form_state['storage']['study_year']-=1;
	} else {
		$form_state['storage']['study_year']+=1;
	}
	$form_state['rebuild']=TRUE;
	return;
}


function theme_prerequisite_form_module_choice($form) {
	$list = array();
	foreach ($form['#years'] as $yname=>$year) {
		if(isset($year['#terms'])) {
			$list[$yname] = array();
			foreach ($year["#terms"] as $tname=>$term) {
				if(isset($term["#modules"])) {
					$modules = $term["#modules"];
					$list[$yname][$tname] = array();
					foreach($modules as $modcode=>$module) {

						if(isset($module['status'])) {
							$item = "";
							#watchdog("Theme mod choice", "checbox array ".var_export($form['modcodes'][$modcode],TRUE));
							
							$item.= drupal_render($form['modcodes'][$modcode]);
							#$item.='<span class="summary">';
							#$item.= drupal_render($module['title']);
							#$item.=" (".drupal_render($module['credits'])." credits)";
							$cls = "status";
							$item.= '<span class="'.$cls.'">'.drupal_render($module['status']).'</span>';
							if($module['#module']['required']) {
								$cls = "required";
							} elseif(!$module["#module"]['available'])  {
								$cls = "unavailable";
							} else {
								$cls = "";
							}
							$pitems = array();
							foreach($module['#prerequisites'] as $pmodcode => $fields)  {
								$pitems[$pmodcode] = array('data'=> drupal_render($fields['title'])." (".drupal_render($fields['term'])." ".drupal_render($fields['year']).') <span class="pre_status"></span>','id'=>"prereq_".$pmodcode);
							}
							if(!empty($pitems)) {
								$item.=theme_item_list($pitems,"Required prerequisites","ul",array("class"=>"prerequisites"));
							}
							$id = "module_".$module['#module']['modcode'];
							$list[$yname][$tname][] = array('data' => $item, 'class'=>$cls, 'id'=>$id);
						} 
					}

				}
			}
		}
	}
	$out="";
	foreach($list as $yname=>$ylist) {
		$yitems=array();
		foreach($ylist as $term=>$items)  {
			$yitems[]=theme_item_list($items,$term." ".$yname,"ul",array("class"=>"term_programme ".strtolower(explode(" ",$term)[0])));
		}
		$out.=theme_item_list($yitems,$yname,"ul",array("class"=>"year_programme ".$yname));
	}	
	drupal_add_js(drupal_get_path('module', 'prerequisite') .'/js/jquery.min.js');
	
	drupal_add_js('$jq.drupal_prerequisite'." = {'selected_modules': ".drupal_to_js($form['#selected_modcodes']).", 'required_modules': ".drupal_to_js($form['#required_modcodes']).", 'prerequisites': ".drupal_to_js($form['#prerequisites']).", 'modules': ".drupal_to_js($form['#modules']).", 'postrequisites': ".drupal_to_js($form['#postrequisites'])."};" ,"inline");
	drupal_add_js(drupal_get_path('module', 'prerequisite') .'/js/jquery.color.min.js');
	drupal_add_js(drupal_get_path('module', 'prerequisite') .'/js/jquery-noconflict.js');
	drupal_add_js(drupal_get_path('module', 'prerequisite') .'/js/prerequisite-public.js');
	drupal_add_css(drupal_get_path('module', 'prerequisite') .'/css/prerequisite-public.css');
	return $out . drupal_render($form);
}

