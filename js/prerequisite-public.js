//var $jq = jQuery.noConflict(true);
$jq(document).ready(function ($) {
	var prereq = $.drupal_prerequisite;
	prereq['checked'] = {};
	var update_prerequisites = function(modcode) {
		if(modcode in prereq.postrequisites ) {
			$.each(prereq.postrequisites[modcode], function(i, postmodcode) {
				console.debug("++ checking postreq "+postmodcode);
				var postmodli = $("li#module_"+postmodcode);
				var postmodcheck = postmodli.find("input.form-checkbox")[0];
				var active = true;

				if(postmodcode in prereq.prerequisites) {
					$.each(prereq.prerequisites[postmodcode], function(i, premodcode) {
						if(!prereq.checked[premodcode]) {
							active = false;
						}
					});
				}
				if(!(postmodcode in prereq.required_modules)) {
					if(active) {
						set_list_entry_available(postmodcheck, true);
					} else {
						set_list_entry_available(postmodcheck, false);
					}
					
				}
				var listel = $(postmodcheck).closest("li");
				render_prerequisite_box(listel);
				update_prerequisites(postmodcode);
			});	
		}
	};		

	var render_prerequisite_box = function(listel) {
		var prereq_els = listel.find(".item-list li");
		$.each(prereq_els, function(i, prelistel)  {
			var modcode = $(prelistel).attr("id").split("_")[1];
			var status = $(prelistel).find(".pre_status");
			if(modcode in prereq.checked) {
				status.addClass("selected");
				status.text("Selected");
			}
			else {
				status.removeClass("selected");
				status.html('<a class="select_prereq" href="#">Select this prerequisite</a>');	
			}
		});
	};

	var set_list_entry_available = function(checkbox, available) {
		var pre_available = $(checkbox).prop("disabled");
		var listel = $(checkbox).closest("li");
		var status = $(listel).find(".status");
		if(available) {
			$(checkbox).prop("disabled", false);
			$(listel).removeClass("unavailable");
			status.html('Prerequisites satisfied (<a href="#show_prereq"></a>)');
		} else {
			$(checkbox).prop("disabled", true);
			checkbox.checked=false;
			update_list_entry(checkbox);
			$(listel).addClass("unavailable");
			status.html('Missing prerequisites (<a href="#show_prereq"></a>)');
			
		}
		set_status_link_text(status,['show','hide']);
		render_prerequisite_box(listel);
	};
	var set_status_link_text = function(status,text) {
		if($(status).closest("li").find(".item-list").is(":hidden")) {
			$(status).find("a").text(text[0]);
		} else {
			$(status).find("a").text(text[1]);
		}
	}
	var update_list_entry = function(checkbox) {
		var checked = $(checkbox).prop('checked');
		var modcode = $(checkbox).val();
		var listel = $(checkbox).closest("li");

		if(checked) {
			prereq.checked[modcode]=modcode;
			$(listel).addClass("selected");

		} else {
			$(listel).removeClass("selected");
			delete prereq.checked[modcode];
		}
	}	

	var get_prereq_el_from_li=function(listel) {
		return get_prereq_el_from_id(get_prereq_id_from_li(listel));
	};

	var get_prereq_el_from_id = function(id) {
		return $("#module_"+id);
	}

	var select_module = function(modcode) {
		var modelt = get_prereq_el_from_id(modcode);
		var checkbox = modelt.find("input.form-checkbox")
		$(checkbox).prop('checked',true);
		update_list_entry(checkbox);
		update_prerequisites(modcode);
		if(modcode in prereq.prerequisites) {
			$.each(prereq.prerequisites[modcode], function(i,premodcode) {
				select_module(premodcode);
			});
		}
	}

	var get_prereq_id_from_li = function(listel) {
		return listel.attr("id").split("_")[1]
	};

	$(".item-list .item-list .item-list").hide();
	$.each(prereq.required_modules, function(i, modcode) {
		$("li#module_"+modcode+" input.form-checkbox").prop("disabled", true);
	});
	$("#prerequisite-form-module-choice").on("change","li input.form-checkbox", function() {
		var checked = $(this).prop('checked');
		var modcode = $(this).val();
		update_list_entry(this);
		update_prerequisites(modcode);
	});
	$("#prerequisite-form-module-choice").on("click","li.unavailable input.form-checkbox", function() {
		
		var checkbox = $(this).find(".form-checkbox");
		var disabled = $(checkbox).prop("disabled");
		
		if(disabled) {
			var status = $(checkbox).closest("li").find(".status");
			var target_color = $.Color(status,"color");
			status.css("color","red");
			status.animate({color: target_color},400);
		}
	});
	$("#prerequisite-form-module-choice").on("click", ".status a", function() {
		var that = this;
		var callback = function() {
			set_status_link_text($(that).closest(".status"),['show','hide']);
		}
		$(this).closest("li").find(".item-list").slideToggle(150, callback);
		return false;
	});

	$("#prerequisite-form-module-choice").on("mouseover", ".prerequisites li", function() {
		var el = get_prereq_el_from_li($(this));
		el.addClass("highlight");
	});

	$("#prerequisite-form-module-choice").on("mouseout", ".prerequisites li", function() {
		var el = get_prereq_el_from_li($(this));
		el.removeClass("highlight");
	});

	$("#prerequisite-form-module-choice").on("click", ".pre_status a", function() {
		var listel = $(this).closest("li");
		select_module(get_prereq_id_from_li(listel));
		return false;
	});

	$.each($("li input.form-checkbox"), function(i, checkbox) {
		update_list_entry(checkbox);
	});
	$.each($("li input.form-checkbox"), function(i, checkbox) {
		var checked = checkbox.checked;
		var modcode = $(checkbox).val();
		update_prerequisites(modcode);
	});
	$.each($("li input.form-checkbox"), function(i, checkbox) {
		update_list_entry(checkbox);
	});



});