jQuery(document).ready(function () {
	var prereq = jQuery.drupal_prerequisite;
	jQuery.each(prereq.required_modules, function(i, modcode) {
		jQuery("li."+modcode+" input.form-checkbox").attr("disabled", true);
	}
});