<?php

################################################################# PROGRAMME STRUCTURE OVERVIEW ################################################

/**
 * Menu callback. return the form for programme structure overview
 */
function prerequisite_admin_programme_archive() {
  $years = prerequisite_get_available_programme_years();
  return theme('prerequisite_admin_programme_archive',array('#years'=>$years, '#next_year'=>prerequisite_get_next_year($years[0])));
}

function theme_prerequisite_admin_programme_archive($data) {
  $next_year = $data['#next_year'];
  $slnext_year = prerequisite_year_slugify($data['#next_year']);
  $header = array('Year','Edit programmes','Update with new modules','Replicate for new year');
  $rows = array();
  foreach($data['#years'] as $year) {
    $slyear = prerequisite_year_slugify($year);
    $row = array();
    $row[] = $year;
    $row[] = l(t('Edit'), "admin/content/progstructure/archive/$slyear");
    $row[] = l(t('Update'), "admin/content/progstructure/update/$slyear", array('query' => 'return=admin/content/progstructure/archive'));
    $row[] = l(t("Replicate for $next_year"), "admin/content/progstructure/replicate/$slyear/$slnext_year", array('query' => 'return=admin/content/progstructure/archive'));
    $rows[] = $row;
  }
  return theme("table",$header,$rows);
}



/**
 * Menu callback. return the form for programme structure overview
 */
function prerequisite_admin_programme_overview_callback($year=FALSE) {
  if(!$year) {
    $year = prerequisite_get_current_year();
  }
  drupal_set_title(t('All programmes (%year)', array('%year' => $year)));
  return drupal_get_form('prerequisite_admin_programme_overview', prerequisite_get_programmes($year), $year);
}


/**
 * Form builder to list and manage programme structures.
 *
 */
function prerequisite_admin_programme_overview(&$form_state, $programmes, $year)  {
  $form = array('#tree' => TRUE);
  $checkboxes = array();
  foreach($programmes as $programme) {
    watchdog("Prerequisite programme overview",var_export($programme,TRUE));
    $form[$programme->pgid]['#programme'] = (array)$programme;
    $checkboxes[$programme->pgid] = ''; 
    $form[$programme->pgid]['title'] = array('#value' => check_plain($programme->title));
    $form[$programme->pgid]['weight'] = array('#type' => 'weight', '#delta' => 10, '#default_value' => $programme->weight);
    $form[$programme->pgid]['edit'] = array('#value' => l(t('Edit structure'), "admin/content/progstructure/$programme->pgid"));
    $form[$programme->pgid]['details'] = array('#value' => l(t('Edit details'), "admin/content/progstructure/details/$programme->pgid"));
    $form[$programme->pgid]['delete'] = array('#value' => l(t('Delete'), "admin/content/progstructure/delete/$programme->pgid"));
  }
  if (count($programmes) > 1) {
    $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  }
  elseif (isset($programme)) {
    unset($form[$programme->pgid]['weight']);
  }
  return $form;
}

/**
 * Theme programme structure overview as a sortable table of programme structures.
 *
 */
function theme_prerequisite_admin_programme_overview($form) {
  $rows = array();
  foreach (element_children($form) as $key) {
    if (isset($form[$key]['title'])) {
      $programme = &$form[$key];
      $row = array();
      $row[] = drupal_render($programme['title']);
      if (isset($programme['weight'])) {
        $programme['weight']['#attributes']['class'] = 'programme-weight';
        $row[] = drupal_render($programme['weight']);
      }
      $row[] = drupal_render($programme['edit']);
      $row[] = drupal_render($programme['details']);
      $row[] = drupal_render($programme['delete']);
      $rows[] = array('data' => $row, 'class' => 'draggable');
    }
  }
  if (empty($rows)) {
    $rows[] = array(array('data' => t('No programmes available.'), 'colspan' => '5'));
  }
  $header = array(t('Name'));
  if (isset($form['submit'])) {
    $header[] = t('Weight');
    drupal_add_tabledrag('programmes', 'order', 'sibling', 'programme-weight');
  }
  $header[] = array('data'=> t('Operations'), 'colspan'=>3);
  return theme('table', $header, $rows, array('id' => 'programmes')) . drupal_render($form);
}

/**
 * Submit handler for programme overview. Updates changed rule weights.
 *
 * @see prerequisite_admin_module_overview()
 */
function prerequisite_admin_programme_overview_submit($form, &$form_state) {
 
    foreach ($form_state['values'] as $pgid => $programme) {
      if (is_numeric($pgid) && $form[$pgid]['#programme']['weight'] != $programme['weight']) {
        $form[$pgid]['#programme']['weight'] = $programme['weight'];
        prerequisite_save_programme($form[$pgid]['#programme']);
      }
    }
  
}



################################################################# PROGRAMME STRUCTURE ################################################

/**
 * Menu callback. return the edit form for programme structure
 */
function prerequisite_admin_progstructure_edit($progstructure) {
  drupal_set_title(t('Edit structure for %programme (%year)', array('%programme' => $progstructure['programme']->title,'%year' => $progstructure['programme']->year)));
  return drupal_get_form('prerequisite_admin_programme_structure', $progstructure['modules']);
}


/**
 * Form builder to edit individual programme structure
 *
 */

function prerequisite_admin_programme_structure(&$form_state, $modules)  {
  $modcodes=array();
  foreach($modules as $module) {
    $form[$module->modcode]['#module'] = (array)$module;
    $modcodes[$module->modcode] = ''; 
    $form[$module->modcode]['title'] = array('#value' => check_plain($module->pmid." ".$module->title));
    $form[$module->modcode]['year'] = array('#value' => check_plain($module->year));
    $form[$module->modcode]['term'] = array('#value' => check_plain(implode(", ",$module->terms)));
    $form[$module->modcode]['modcode'] = array('#value' => check_plain($module->modcode));
    $form[$module->modcode]['status'] = array('#value' => check_plain($module->status));
  }
  $form['modcodes'] = array
  (
    '#type' => 'checkboxes',
    '#options' => $modcodes, 
  );
  if (count($modules)) {
    $form['actions'] = array('#type'=>'select','#options' => array('set_required'=>"Set selected as required","set_optional"=>"Set selected as optional","set_available"=>"Set selected as available","set_unavailable"=>"Set selected as unavailable"));
    $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  }
  return $form;
}

/**
 * Theme structure details as table of modules
 *
 */
function theme_prerequisite_admin_programme_structure($form) {
  $rows = array();
  foreach (element_children($form) as $key) {
    if (isset($form[$key]['title'])) {
      watchdog("Key",$key);
      $module = &$form[$key];
      $row = array();
      $row[] = drupal_render($form['modcodes'][$key]); 
      watchdog("Key",var_export($form['modcodes'][$key], TRUE));
      $row[] = drupal_render($module['title']);
      $row[] = drupal_render($module['year']);
      $row[] = drupal_render($module['term']);
      $row[] = drupal_render($module['modcode']);
      $row[] = drupal_render($module['status']);
      $rows[] = array('data' => $row);
    }
  }
  if (empty($rows)) {
    $rows[] = array(array('data' => t('No modules available.'), 'colspan' => '6'));
  } 
  $header = array(theme('table_select_header_cell'), t('Name'), t('Year'),t('Term'),t('Module code'),t('Status'));
  return theme('table', $header, $rows, array('id' => 'progstructure')) . drupal_render($form);
}

/**
 * Submit handler for programme structure. 
 */
function prerequisite_admin_programme_structure_submit($form, &$form_state) {
  foreach($form_state['values']['modcodes'] as $modcode => $selected) {
    if($selected) {
      $module = $form[$modcode]["#module"];
      switch($form_state['values']['actions']) {
        case 'set_available':
          $module['available']=1;
          unset($module['required']);
          break;
        case 'set_unavailable':
          $module['available']=0;
          $module['required']=0;
          break;
        case 'set_required':
          $module['required']=1;
          $module['available']=1;
          break;
        case 'set_optional':
          $module['required']=0;
          $module['available']=1;
          break;
      }
      if(isset($module['pmid']) && $module['pmid']===NULL) {
        unset($module['pmid']);
      }
      prerequisite_save_programme_module($module);
    }
  }
}


function prerequisite_admin_update_programmes($year) {
  $programmes = prerequisite_get_programmes($year);
  foreach($programmes as $programme) {
    watchdog("Admin prog",var_export($programme,TRUE));
    prerequisite_generate_programme_structure($programme->pgid,$programme->year);
  }
  drupal_goto($_GET['return']);
}

function prerequisite_admin_replicate_programmes($year1, $year2) {
  watchdog("repl",$year1." ".$year2);
  prerequisite_replicate_programmes($year1, $year2);
  prerequisite_replicate_programme_structures($year1, $year2);
  prerequisite_admin_update_programmes($year2);
}




######################################################  PROGRAMME DETAILS #######################################################

/**
 * Form builder for programme details
 */

function prerequisite_form_programme(&$form_state, $edit = array()) {

  if(!is_array($edit) && is_object($edit)) {
    $edit = (array) $edit;
  }
  watchdog("Programme form","start");
  $edit += array(
    'ucascode' => '',
    'pgid' => NULL,
    'weight' => 0,
    'title' => '',
    'duration' => 3,
    'year' => prerequisite_get_current_year()
  );

  $form['programme'] = array(
    '#type' => 'fieldset',
    '#title' => t('Programme details'),
    '#collapsible' => FALSE,
  );
  $form['programme']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Programme name'),
    '#default_value' => $edit['title'],
    '#maxlength' => 200,
  );
  $form['programme']['ucascode'] = array(
    '#type' => 'textfield',
    '#title' => t('UCAS code'),
    '#default_value' => $edit['ucascode'],
    '#maxlength' => 20,
  );
  $form['programme']['duration'] = array(
    '#type' => 'textfield',
    '#title' => t('Programme length in years'),
    '#default_value' => $edit['duration'],
    '#maxlength' => 10,
  );
  $form['year'] = array(
    '#type' => 'value',
    '#value' => $edit['year'] 
  );

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
    '#collapsible' => TRUE,
  );


  $form['settings']['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => $edit['weight'],
    '#description' => t('Programmes are displayed in ascending order by weight.'),
  );
  if (isset($edit['pgid'])) {
    $form['delete'] = array('#type' => 'submit', '#value' => t('Delete'));
    $form['pgid'] = array('#type' => 'value', '#value' => $edit['pgid']);
  }
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  watchdog("Programme form","end");
  return $form;
}

function prerequisite_form_programme_validate($form, &$form_state) {
  if (isset($form_state['values']['weight']) && !is_numeric($form_state['values']['weight'])) {
    form_set_error('weight', t('Weight value must be numeric.'));
  }
  if (isset($form_state['values']['duration']) && !is_numeric($form_state['values']['duration'])) {
    form_set_error('duration', t('Course length value must be numeric.'));
  }
  if (!isset($form_state['values']['year'])) {
    $form_state['values']['year'] = prerequisite_get_current_year();
  }
}


/**
 * Submit handler to insert or update a programme's details.
 */
function prerequisite_form_programme_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#value'] == t('Delete')) {
    // Execute the term deletion.

    if ($form_state['values']['delete'] === TRUE) {
      return prerequisite_programme_confirm_delete_submit($form, $form_state);
    }
    // Rebuild the form to confirm term deletion.
    $form_state['rebuild'] = TRUE;
    $form_state['confirm_delete'] = TRUE;
    return;
  }
  switch (prerequisite_save_programme($form_state['values'])) {
    case SAVED_NEW:
      drupal_set_message(t('Created new programme structure for %title.', array('%title' => $form_state['values']['title'])));
      watchdog('prerequisite', 'Created new programme structure for %title.', array('%title' => $form_state['values']['title']), WATCHDOG_NOTICE, l(t('edit'), 'admin/content/progstructure/edit/'. $form_state['values']['pgid']));
      break;
    case SAVED_UPDATED:
      drupal_set_message(t('Updated programme details for %title.', array('%title' => $form_state['values']['title'])));
      watchdog('taxonomy', 'Updated programme details for %title.', array('%title' => $form_state['values']['title']), WATCHDOG_NOTICE, l(t('edit'), 'admin/content/progstructure/edit/'. $form_state['values']['pgid']));
      break;
  }

}

################################################################# PREREQUISITE RULE OVERVIEW ################################################

/**
 * Menu callback. return the edit form for all rules in module
 */
function prerequisite_admin_rules_module_edit($rules) {
  drupal_set_title(t('Edit prerequisites for %module', array('%module' => $rules['parent']->title)));
  return drupal_get_form('prerequisite_admin_module_overview' , $rules['rules']);
}


/**
 * Form builder to list and manage rules.
 *
 */
function prerequisite_admin_module_overview(&$form_state, $rules) {
  $form = array('#tree' => TRUE);
  foreach($rules as $rule) {
    $types = array();

    $form[$rule->prid]['#rule'] = (array)$rule;
    $form[$rule->prid]['title'] = array('#value' => check_plain($rule->title));
    $form[$rule->prid]['pmodcode'] = array('#value' => check_plain($rule->pmodcode));
    $form[$rule->prid]['weight'] = array('#type' => 'weight', '#delta' => 10, '#default_value' => $rule->weight);
    $form[$rule->prid]['edit'] = array('#value' => l(t('Delete'), "admin/content/prerequisite/delete/$rule->prid"));
  }

  // Only make this form include a submit button and weight if more than one
  // rule exists.
  if (count($rules) > 1) {
    $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  }
  elseif (isset($rule)) {
    unset($form[$rule->prid]['weight']);
  }
  return $form;
}


/**
 * Submit handler for module rules overview. Updates changed rule weights.
 *
 * @see prerequisite_admin_module_overview()
 */
function prerequisite_admin_module_overview_submit($form, &$form_state) {
  foreach ($form_state['values'] as $prid => $rule) {
    watchdog("Prereq", "ID: ".(string)$prid);
    if (is_numeric($prid) && $form[$prid]['#rule']['weight'] != $rule['weight']) {
      $form[$prid]['#rule']['weight'] = $rule['weight'];
      prerequisite_save_rule($form[$prid]['#rule']);
    }
  }
}

/**
 * Theme the module overview as a sortable list of prerequisite rules.
 *
 * @ingroup themeable
 * @see prerequisite_admin_module_overview()
 */
function theme_prerequisite_admin_module_overview($form) {
  $rows = array();
  foreach (element_children($form) as $key) {
    if (isset($form[$key]['title'])) {

      $rule = &$form[$key];

      $row = array();

      $row[] = drupal_render($rule['title']);
      $row[] = drupal_render($rule['pmodcode']);
      if (isset($rule['weight'])) {
        $rule['weight']['#attributes']['class'] = 'rule-weight';
        $row[] = drupal_render($rule['weight']);
      }
      $row[] = drupal_render($rule['edit']);
      $rows[] = array('data' => $row, 'class' => 'draggable');
    }
  }
  if (empty($rows)) {
    $rows[] = array(array('data' => t('No rules available.'), 'colspan' => '3'));
  }

  $header = array(t('Name'), t('Module'));
  if (isset($form['submit'])) {
    $header[] = t('Weight');
    drupal_add_tabledrag('prerequisite', 'order', 'sibling', 'rule-weight');
  }
  $header[] = array('data' => t('Operations'));
  return theme('table', $header, $rows, array('id' => 'prerequisite')) . drupal_render($form);
}
##################################################################### PREREQUISITE RULE ################################################

/**
 * Menu callback. return the edit form for a new rule after setting the title.
 */
function prerequisite_admin_rule_add($module) {
  drupal_set_title(t('Add prerequisite to %module', array('%module' => $module->title)));
  return drupal_get_form('prerequisite_form_rule' , $module->modcode);
}


/**
 * Form builder for rule
 */
function prerequisite_form_rule(&$form_state, $modcode, $edit = array())  {
  $edit += array(
    'pmodcode' => '',
    'prid' => NULL,
    'weight' => 0,
  );

  if (isset($form_state['confirm_delete'])) {
    return prerequisite_rule_confirm_delete($form_state, $edit['prid']);
  }

  $form['course_module'] = array(
    '#type' => 'fieldset',
    '#title' => t('Prerequisite module'),
    '#collapsible' => FALSE,
    '#description' => t('Either a) enter the UoY module code manually or b) start typing the course title to generate a dropdown list of possible modules, and select one')
  );

  $form['course_module']['pmodcode'] = array(
    '#type' => 'textfield',
    '#title' => t('Module code'),
    '#default_value' => $edit['pmodcode'],
    '#maxlength' => 128,
    '#autocomplete_path' => 'admin/prerequisite/autocomplete',
  );

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
    '#collapsible' => TRUE,
  );

  $form['settings']['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => $edit['weight'],
    '#description' => t('Prerequisite rules are displayed in ascending order by weight.'),
  );

  $form['modcode'] = array(
    '#type' => 'value',
    '#value' => $modcode);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'));

  if (isset($edit['prid'])) {
    $form['delete'] = array('#type' => 'submit', '#value' => t('Delete'));
    $form['prid'] = array('#type' => 'value', '#value' => $edit['prid']);
  }
  return $form;
}


function prerequisite_form_rule_validate($form, &$form_state) {
  if (isset($form_state['values']['weight']) && !is_numeric($form_state['values']['weight'])) {
    form_set_error('weight', t('Weight value must be numeric.'));
  }
}

/**
 * Submit handler to insert or update a prerequisite.
 *
 * @see prerequisite_form_rule()
 */
function prerequisite_form_rule_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#value'] == t('Delete')) {
    // Execute the term deletion.
    watchdog("Prerequisite form rule","Delete clicked.");
    if ($form_state['values']['delete'] === TRUE) {
      watchdog("Prerequisite form rule","Value delete is there");
      return prerequisite_rule_confirm_delete_submit($form, $form_state);
    }
    // Rebuild the form to confirm term deletion.
    $form_state['rebuild'] = TRUE;
    $form_state['confirm_delete'] = TRUE;
    return;
  }
  switch (prerequisite_save_rule($form_state['values'])) {
    case SAVED_NEW:
      drupal_set_message(t('Created new prerequisite for %modcode: %pmodcode.', array('%modcode' => $form_state['values']['modcode'],'%pmodcode' => $form_state['values']['pmodcode'])));
      watchdog('prerequisite', 'Created new prerequisite for %modcode: %pmodcode.', array('%modcode' => $form_state['values']['modcode'],'%pmodcode' => $form_state['values']['pmodcode']), WATCHDOG_NOTICE, l(t('edit'), 'admin/content/prerequisite/edit/'. $form_state['values']['prid']));
      $form_state['redirect'] = 'admin/content/prerequisite/'.$form_state['values']['modcode'];
      break;
    case SAVED_UPDATED:
      drupal_set_message(t('Updated prerequisite for %modcode: %pmodcode.', array('%modcode' => $form_state['values']['modcode'],'%pmodcode' => $form_state['values']['pmodcode'])));
      watchdog('taxonomy', 'Updated prerequisite for %modcode: %pmodcode.', array('%modcode' => $form_state['values']['modcode'],'%pmodcode' => $form_state['values']['pmodcode']), WATCHDOG_NOTICE, l(t('edit'), 'admin/content/taxonomy/edit/'. $form_state['values']['prid']));
      $form_state['redirect'] = 'admin/content/prerequisite/'.$form_state['values']['modcode'];
      break;
  }

}

############################################# PREREQUISITE RULE DELETE ################################################

/**
 * Menu callback for rule deletion. return the delete form for a rule after setting the title.
 */
 
function prerequisite_admin_delete($prid) {
  if ($rule = (array)prerequisite_get_rule($prid)) {
    return drupal_get_form('prerequisite_rule_confirm_delete', $rule['prid']);
  }
  return drupal_not_found();
}
/**
 * Form builder for the prerequisite rule delete form.
 *
 * @ingroup forms
 * @see prerequisite_rule_confirm_delete_submit()
 */
function prerequisite_rule_confirm_delete(&$form_state, $prid) {
  $rule = prerequisite_get_rule($prid);

  $form['type'] = array('#type' => 'value', '#value' => 'rule');
  $form['modcode'] = array('#type' => 'value', '#value' => $rule->modcode);
  $form['pmodcode'] = array('#type' => 'value', '#value' => $rule->pmodcode);
  $form['prid'] = array('#type' => 'value', '#value' => $prid);
  $form['delete'] = array('#type' => 'value', '#value' => TRUE);
  return confirm_form($form,
                  t('Are you sure you want to delete the prerequisite %pmod for %mod?',
                  array('%pmod' => $rule->pmodcode,'%mod'=> $rule->modcode)),
                  'admin/content/prerequisite/'.$rule->modcode,
                  t('This action cannot be undone.'),
                  t('Delete'),
                  t('Cancel'));
}

/**
 * Submit handler to delete a term after confirmation.
 *
 * @see prerequisite_rule_confirm_delete()
 */
function prerequisite_rule_confirm_delete_submit($form, &$form_state) {
  watchdog("Prerequisite form rule delete submit","Deleting.");
  prerequisite_del_rule($form_state['values']['prid']);
  drupal_set_message(t('Deleted prerequisite for %modcode: %pmodcode.', array('%modcode' => $form_state['values']['modcode'],'%pmodcode' => $form_state['values']['pmodcode'])));
  watchdog('prerequisite', t('Deleted prerequisite for %modcode: %pmodcode.', array('%modcode' => $form_state['values']['modcode'],'%pmodcode' => $form_state['values']['pmodcode'])), WATCHDOG_NOTICE);
  $form_state['redirect'] = 'admin/content/prerequisite/'.$form_state['values']['modcode'];
  return;
}



####################################################################################    PROGRAMME STRUCTURE ################################

/**
 * Form builder for the programme structure delete form.
 *
 * @ingroup forms
 * @see prerequisite_progstructure_confirm_delete_submit()
 */
function prerequisite_progstructure_delete_form(&$form_state, $pgid) {
  $programme = prerequisite_programme_load($pgid);
  watchdog("Prerequisite programme delete form", var_export($programme,TRUE));

  $form['type'] = array('#type' => 'value', '#value' => 'rule');
  $form['ucascode'] = array('#type' => 'value', '#value' => $programme->ucascode);
  $form['title'] = array('#type' => 'value', '#value' => $programme->title);
  $form['pgid'] = array('#type' => 'value', '#value' => $pgid);
  $form['delete'] = array('#type' => 'value', '#value' => TRUE);
  return confirm_form($form,
                  t('Are you sure you want to delete the programme structure for %title (%ucascode)?',
                  array('%title' => $programme->title,'%ucascode'=> $programme->ucascode)),
                  'admin/content/progstructure',
                  t('This action cannot be undone.'),
                  t('Delete'),
                  t('Cancel'));
}


  /**
 * Submit handler to delete a programme structure after confirmation.
 *
 * @see prerequisite_progstructure_confirm_delete()
 */
function prerequisite_progstructure_delete_form_submit($form, &$form_state) {
  watchdog("Prerequisite progstructure delete submit","Deleting ".$form_state['values']['pgid']);
  prerequisite_del_programme($form_state['values']['pgid']);
  drupal_set_message(t('Deleted programme structure for %title', array('%title' => $form_state['values']['title'])));
  $form_state['redirect'] = 'admin/content/progstructure/';
  return;
}

/**
 * Form builder for the prerequisite rule delete form.
 *
 * @ingroup forms
 * @see prerequisite_rule_confirm_delete_submit()
*/


function prerequisite_programme_confirm_delete(&$form_state, $pgid) {
  $rule = prerequisite_programme_load($pgid);

  $form['type'] = array('#type' => 'value', '#value' => 'rule');
  $form['ucascode'] = array('#type' => 'value', '#value' => $rule->ucascode);
  $form['title'] = array('#type' => 'value', '#value' => $rule->title);
  $form['pgid'] = array('#type' => 'value', '#value' => $pgid);
  $form['delete'] = array('#type' => 'value', '#value' => TRUE);
  return confirm_form($form,
                  t('Are you sure you want to delete the programme structure for %title?',
                  array('%title' => $rule->title,)),
                  'admin/content/progstructure/',
                  t('This action cannot be undone.'),
                  t('Delete'),
                  t('Cancel'));
}



############################################################################# SETTINGS #####################################################


function prerequisite_settings_form(&$form_state) {
  $form=array();
  $form['prerequisite_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => variable_get('prerequisite_title','Interactive module prerequisite tool'),
    '#size' => 200,
    '#maxlength' => 200,
    '#description' => t('Title displayed on each interactive module page.'),
    '#required' => FALSE
  );
  $headervalue = variable_get('prerequisite_intro', array('body' => '<p><strong>This tool is provided for guidance on prerequisites for mathematics modules only</strong>.  It does not give you information on all the options available to you when choosing your modules. Please consult the full programme page for this module (see programme descriptions) for full details, and discuss with your supervisor.</p>', 'format' => FILTER_FORMAT_DEFAULT));

  $form['prerequisite_intro'] = array(
    '#type'  => 'item',
    '#tree'   => TRUE,
  );
  $form['prerequisite_intro']['body'] = array(
    '#type' => 'textarea',
    '#title' => t('Introductory text'),
    '#rows'  =>  8,
    '#default_value' => $headervalue['body'],
    '#description' => t('Introductory text displayed on each interactive module page.'),
  );

  $form['prerequisite_intro']['format'] = filter_form($headervalue['format'], NULL, array('prerequisite_intro', 'format'));
  return system_settings_form($form);
}


?>